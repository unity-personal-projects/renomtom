=================================================
||             Application RenomTom            ||
||            Créée par BERNE Thomas           ||
=================================================
Liste des fichiers :

 - Il faut lancer le RenomTom.py pour démarrer l'application. version python : 3.6

 - Il faut aller dans build pour avoir l'executable. Créé avec le fichier Build.py

 - Le  fichier classe.py contient toutes les classes créées mis à part celle de l'interface.

 - Le fichier Fenetre.py contient la classe de l'interface.

 - Le fichier RenomTom_sauvegarde.ini contient les sauvegardes des règles entrées.

 - Le fichier Test.py s'occupe des tests Unitaires.

=================================================

Objectif application : Application de renommage de fichiers avec une liste de rêgles prédéfinis.

TODO : 
Refactoriser le code.
Revoir le fonctionnement.
